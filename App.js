
// Sert a ordonner l'affichage des vues dans l'application
// Et la gestion des différentes polices


import React, {useRef} from "react";
import { StyleSheet, View } from "react-native";
// import de font loader
import { useFonts } from "expo-font";

// import du store de l'application
import store from "./src/store";
import { Provider } from "react-redux";

// import du composant de la navigation
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

// import des différents écrans
import HomeScreen from "./screens/home-screen";
import MoreScreen from "./screens/more-screen";
import color from "./src/color";
import LottieView from 'lottie-react-native';

import { Dimensions } from "react-native";

// Récupération de la largeur de l'écran
const { width } = Dimensions.get("screen");

export default function App() {
  // Chargement des polices
  const [fontsLoaded] = useFonts({
    "n_b": require("./assets/fonts/n_b.ttf"),
    "n_sb": require("./assets/fonts/n_sb.ttf"),
    "n_r": require("./assets/fonts/n_r.ttf"),
  });
  const animation = useRef(null);
  if (!fontsLoaded) {
    return <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
      <LottieView
        autoPlay
        ref={animation}
        style={{
          width: width,
          height: width,
        }}
        source={require('./assets/progress_rocket.json')}
      />
    </View>
  }

  const Stack = createNativeStackNavigator();

  // build stack navigation
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="home-screen"
            component={HomeScreen}
            options={{
              header: () => null
            }}
          />
          <Stack.Screen
            name="more-screen"
            component={MoreScreen}
            options={{
              header: () => null,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}


const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    marginTop: 40,
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginBottom: 10,
  },
  name: {
    fontSize: 24,
    fontFamily: 'n_b',
    color: color.dark
  },
  headerIcon: {
    width: 30,
    height: 30,
    tintColor: color.dark,
  }
})