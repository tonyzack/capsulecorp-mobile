// Vue informations capsule
// Il récupère les données de la capsule sauvegardées en mémoire

import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
  StatusBar,
} from "react-native";
import React from "react";
import color from "../src/color";
import { RenderStatusText, RenderTypeText } from "../components/capsule-row";

import { Dimensions } from "react-native";

const { width } = Dimensions.get("screen");

const MoreScreen = ({ route, navigation }) => {
  const { capsule } = route.params;

  return (
    <View style={styles.container}>
      <StatusBar translucent={true} barStyle={"light-content"} backgroundColor={"#000000"}/>
      <ScrollView>
        <View style={styles.header}>
          {/* Affichage de l'image de fond à l'écran de détail */}
          <ImageBackground 
            source={require("../assets/images/capsule.jpg")}
            resizeMode="cover"
            style={styles.headerImage}
          >
            <View style={styles.aplat}></View>
            <View
              style={{
                position: "absolute",
                zIndex: 3,
                bottom: -110,
                start: 10,
                flexDirection: "row",
                alignItems: "flex-end",
              }}
            >
              {/* Composant de l'image de fusée */}
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  width: 170,
                  height: 200,
                }}
              >
                <Image
                  style={{ width: 170, height: 200 }}
                  source={require("../assets/images/card_shadowed.png")}
                />
                <Image
                  style={{ width: 120, height: 150, position: "absolute" }}
                  source={require("../assets/images/cap.png")}
                />
                {/* Type de la capsule */}
                <RenderTypeText type={capsule.type} />
              </View>
              {/* Information sur le nom, le status, la dernière mise à jour */}
              <View style={styles.infoContent}>
                <Text style={styles.serial}>{capsule.serial}</Text>
                <RenderStatusText status={capsule.status} />
                <Text style={styles.desc} numberOfLines={2}>
                  {capsule.last_update}
                </Text>
              </View>
            </View>
          </ImageBackground>
        </View>
        <View style={{ marginTop: 140 }}></View>
        {/* Information sur le nombre d'utilisation */}
        <View style={styles.row}>
          <View style={styles.field}>
            <Image
              source={require("../assets/images/recycle.png")}
              style={styles.image}
            />
            <Text style={styles.text}>Reuse</Text>
          </View>
          <View style={styles.value}>
            <Text style={styles.info}>
              {capsule.reuse_count > 0 && "x"}
              {capsule.reuse_count}
            </Text>
          </View>
        </View>
        {/* Information sur le nombre d'attérissage dans l'eau */}
        <View style={styles.row}>
          <View style={styles.field}>
            <Image
              source={require("../assets/images/wave.png")}
              style={styles.image}
            />
            <Text style={styles.text}>Water Landings</Text>
          </View>
          <View style={styles.value}>
            <Text style={styles.info}>
              {capsule.water_landings > 0 && "x"}
              {capsule.water_landings}
            </Text>
          </View>
        </View>
        {/* Inforamtion sur le nombre d'attérissage sur terre */}
        <View style={styles.row}>
          <View style={styles.field}>
            <Image
              source={require("../assets/images/land.png")}
              style={styles.image}
            />
            <Text style={styles.text}>Land Landings</Text>
          </View>
          <View style={styles.value}>
            <Text style={styles.info}>
              {capsule.land_landings > 0 && "x"}
              {capsule.land_landings}
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default MoreScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  header: {},
  headerImage: {
    height: 300,
    position: "relative",
    justifyContent: "space-between",
  },
  aplat: {
    position: "absolute",
    backgroundColor: "rgba(0, 0, 0, .3)",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 1,
  },
  nameContainer: {
    zIndex: 2,
    backgroundColor: color.white + "66",
    width: 200,
    borderRadius: 100,
    marginTop: 20,
    marginLeft: 20,
  },
  infoContent: {
    marginBottom: 8,
    alignItems: 'flex-start',
    marginLeft: 10,
  },
  desc: {
    color: color.dark + "aa",
    fontFamily: "n_r",
    marginVertical: 5,
    width: width - 190,
  },
  stat: {
    zIndex: 2,
    marginLeft: 50,
    marginBottom: 10,
    flexDirection: "row",
    backgroundColor: color.white + "99",
    width: 180,
    borderRadius: 4,
  },
  status: {
    width: 80,
    marginRight: 20,
  },
  serial: {
    color: color.dark,
    fontSize: 23,
    fontFamily: "n_b",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 20,
    alignItems: "center",
    marginVertical: 12,
  },
  image: {
    width: 40,
    height: 40,
  },
  field: {
    flexDirection: "row",
    alignItems: "center",
  },
  value: {},
  text: {
    fontSize: 18,
    marginLeft: 20,
    color: color.dark + "88",
    fontFamily: "n_sb",
  },
  info: {
    fontSize: 22,
    color: color.dark,
    fontFamily: "n_b",
  },
});
