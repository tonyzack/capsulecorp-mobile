// Vue de l'écran d'accueil - affiche la liste des capsules

import {
  FlatList,
  Image,
  StyleSheet,
  StatusBar,
  Text,
  View,
  Pressable,
} from "react-native";
import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as Constants from "expo-constants";
import LottieView from "lottie-react-native";

import getCapsules from "../src/api";
import { addCapsules } from "../src/store";
import Touchable from "../components/touchable";
import color from "../src/color";
import CapsuleRow from "../components/capsule-row";

import { Dimensions } from "react-native";

const { width } = Dimensions.get("screen");

const HomeScreen = ({ route, navigation }) => {
  const store = useSelector((state) => state);
  const dispatch = useDispatch();
  const [state, setState] = useState({ capsule: [], loading: false, error: false });
  const animation = useRef(null);

  // Récupération des données capsules
  async function getData() {
    setState((state) => ({
      ...state,
      loading: true,
    }));
    let data = null
    try {
      data = await getCapsules();
    } catch(e) {}
    dispatch(addCapsules(data ? data : []));
    setState((state) => ({
      capsule: data ? data : [],
      loading: false,
      error: data ? false : true
    }));
  }
  

  useEffect(() => {
    getData()
    return () => {};
  }, []);


  // Affiche la vue de chargement (lors de la récupération des données)
  if (state.loading) {
    return (
      <View
        style={{
          backgroundColor: "#fff",
          justifyContent: "center",
          alignItems: "center",
          flex: 1,
        }}
      >
        <LottieView
          autoPlay
          ref={animation}
          style={{
            width: width,
            height: width,
          }}
          source={require("../assets/progress_rocket.json")}
        />
      </View>
    );
  }

  // Affiche la vue erreur (Ex: Connexion impossible à l'api, etc...)
  if(state.error){
    return (
      <Pressable 
        onPress={() => {getData()}}
        style={{
          backgroundColor: "#fff",
          justifyContent: "center",
          alignItems: "center",
          flex: 1,
        }}
        >
          <View
          style={{
            justifyContent: "center",
            alignItems: "center",
          }}
          >
          <Text>Impossible de se connecter. Veuillez réessayer</Text>
          <Text style={{color:color.dark, fontFamily:'n_sb', fontSize:18, top:25}}>RÉESSAYER</Text>
        </View>
      </Pressable>
    );
  }
  const onPressCapsuleMoreButton = (item) => {
    navigation.navigate("more-screen", { capsule: item });
  };

  // Lister les données capsules
  return (
    <View style={styles.container}>
      <StatusBar
        translucent={false}
        backgroundColor={"#FFFFFF"}
        barStyle={"dark-content"}
      />
      {/* Affichage de la liste des capsules */}
      <FlatList
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        overScrollMode={"never"}
        data={state.capsule}
        renderItem={({ item, index }) => (
          // Une ligne de la liste des capsules
          <Touchable onPress={() => onPressCapsuleMoreButton(item)}>
            <View style={{ flex: 1 }}>
              <CapsuleRow
                capsule={item}
                onPress={() => onPressCapsuleMoreButton(item)}
              />
            </View>
          </Touchable>
        )}
        ListHeaderComponent={
          // l'entête de la liste des capsules 
          <View>
            <View style={styles.header1}>
              <Text style={styles.name}>Capsule Corp</Text>
              <View
                style={{
                  backgroundColor: color.shadow,
                  borderRadius: 12,
                  padding: 8,
                }}
              >
                <Image
                  source={require("../assets/images/ic_filter.png")}
                  style={styles.headerIcon}
                />
              </View>
            </View>
            {/* la bare de recherche */}
            <View style={styles.header2}>
              <Touchable>
                <View style={styles.search}>
                  <Image
                    source={require("../assets/images/ic_search.png")}
                    style={styles.searchIcon}
                  />
                  <Text style={styles.searchInput}>Rechercher</Text>
                </View>
              </Touchable>
            </View>
          </View>
        }
      />
    </View>
  );
};

export default HomeScreen;

let topSpace = 0;
if (Constants.default.platform.ios) topSpace = 80;
else topSpace = 40;

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.white,
    flex: 1,
  },
  header1: {
    flexDirection: "row",
    marginTop: topSpace,
    alignItems: "flex-end",
    justifyContent: "space-between",
    marginHorizontal: 20,
    marginBottom: 10,
  },
  name: {
    fontSize: 28,
    fontFamily: "n_b",
    color: color.dark,
  },
  headerIcon: {
    width: 30,
    height: 30,
    tintColor: color.dark,
  },
  header2: {
    height: 75,
    backgroundColor: color.white,
    borderColor: color.dark + "19",
    borderWidth: 1,
    borderTopWidth: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    marginTop: -10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  search: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: color.shadow,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    paddingVertical: 12,
    borderRadius: 8,
    width: width - 30,
  },
  searchIcon: {
    width: 20,
    height: 20,
    marginRight: 10,
    tintColor: color.base2,
  },
  searchInput: {
    // borderWidth: 1,
    // width: "85%",
    fontSize: 16,
    fontFamily: "n_b",
    color: color.base2,
  },
});
