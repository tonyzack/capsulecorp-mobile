// Création de l'état global du store des capsules et 
// des différentes fonctions de mutation du store 

import { configureStore } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";


export const capsuleSlice = createSlice({
  name: "capsule",
  initialState: [],
  reducers: {
    addCapsules: (state, action) => {
      return [...state, ...action.payload];
    },
    getCapsules: (state, action) => {},
    removeCapsules: (state, action) => {},
  },
});

export const { addCapsules, getCapsules, removeCapsules } =
  capsuleSlice.actions;

export default configureStore({
  reducer: {
    capsule: capsuleSlice.reducer,
  },
});
