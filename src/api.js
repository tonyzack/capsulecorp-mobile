// Utilités requettes api

import axios from "axios";

const entryPoint = "https://api.spacexdata.com/v4/capsules";

// Fonction de requette HTTP
async function getFetch() {
  let response = await axios({
    url: entryPoint,
    method: "GET",
  });
  if (response.status == 200) {
    return response.data;
  }
  return null;
}

/**
 * Fonction de récupération des capsules
 * @param {string} query
 * @param {number} offset
 * @param {number} limit
 * @param {{filed: string, by: string}} order
 * @returns any[]
 */
export default async function getCapsules() {
  let data = await getFetch();
  return data;
}
