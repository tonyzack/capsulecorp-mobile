// les différentes couleurs utilisées dans l'application
export default {
    primary: '#cb0c9f',
    secondary: '#cb0c9f',
    third: '#f8d210',
    fourth: '#fa26a0',
    five: '#00FF55',
    succes: '#2ff3e0',
    warning: '#ff9f1c',
    danger: '#f51720',
    shadow: '#acb5ae33',
    dark: '#050a30',
    white: '#FFFFFF',
    gray: '#7ec8e3',
    base1: "#19686868",
    base2: "#696969"
}