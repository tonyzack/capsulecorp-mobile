
// Permet de créer le composant pour afficher une ligne de la 
// liste des capsules

import { Image, StyleSheet, Text, View } from "react-native";
import React from "react";
import color from "../src/color";
import Touchable from "./touchable";

// Composant pour afficher l'iicon du statu de la capsule
export const RenderStatusIcon = ({ status }) => {
  return status == "unknown" ? (
    <View>
      <Image
        source={require("../assets/images/unknown.png")}
        style={styles.statusIcon}
      />
    </View>
  ) : status == "active" ? (
    <View>
      <Image
        source={require("../assets/images/activity.png")}
        style={styles.statusIcon}
      />
    </View>
  ) : status == "destroyed" ? (
    <View>
      <Image
        source={require("../assets/images/delete.png")}
        style={styles.statusIcon}
      />
    </View>
  ) : status == "retired" ? (
    <View>
      <Image
        source={require("../assets/images/remove-text.png")}
        style={styles.statusIcon}
      />
    </View>
  ) : null;
};

// Composant pour afficher le statu de la capsule
export function RenderStatusText({ status }) {
  const textStyle = {
    ...styles.text,
    ...styles.textBold,
    textAlign: "center",
  };
  const viewStyle = { paddingHorizontal: 5, borderRadius: 4, flex: 0 };
  return status == "unknown" ? (
    <View style={[viewStyle, { backgroundColor: color.secondary + "19" }]}>
      <Text style={[textStyle, { color: color.secondary }]}>{status}</Text>
    </View>
  ) : status == "active" ? (
    <View style={[viewStyle, { backgroundColor: color.five + "19" }]}>
      <Text style={[textStyle, { color: color.five }]}>{status}</Text>
    </View>
  ) : status == "destroyed" ? (
    <View style={[viewStyle, { backgroundColor: color.danger + "19" }]}>
      <Text style={[textStyle, { color: color.danger }]}>{status}</Text>
    </View>
  ) : status == "retired" ? (
    <View style={[viewStyle, { backgroundColor: color.warning + "19" }]}>
      <Text style={[textStyle, { color: color.warning }]}>{status}</Text>
    </View>
  ) : null;
}

// Composant pour afficher le type de la capsule
export function RenderTypeText({ type }) {
  const textStyle = { ...styles.text, fontFamily: "n_sb" };
  return type.indexOf("2") != -1 ? (
    <Text style={[textStyle, { color: color.primary }]}>{type}</Text>
  ) : (
    <Text style={[textStyle, , { color: color.gray }]}>{type}</Text>
  );
}
const CapsuleRow = ({ capsule, onPress }) => {
  let { id, status, type, serial } = capsule;
  return (
    <View style={styles.container}>
      {/* Affichage de l'icon, du nom et du statu */}
      <View style={styles.flexRow}>
        <View style={{ flexDirection: "row", flex: 1 }}>
          <View>
            <RenderStatusIcon status={status} />
          </View>
          <View style={[styles.status]}>
            <Text style={[styles.text, styles.id]}>{serial}</Text>
            <RenderStatusText status={status} />
          </View>
        </View>
        <View style={styles.status}>
          {/* <Text style={styles.text}>type</Text> */}
          {/* <RenderTypeText type={type} /> */}
        </View>
        {/* Affichage du button voir plus */}
        <Touchable onPress={onPress}>
          <View style={styles.imageContainer}>
            <Image
              source={require("../assets/images/more-than.png")}
              style={styles.image}
            />
          </View>
        </Touchable>
      </View>
    </View>
  );
};

export default CapsuleRow;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    position: "relative",
  },
  flexRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 10,
    marginVertical: 5,
  },
  status: {
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "flex-start",
    marginLeft: 20,
  },
  text: {
    fontFamily: "n_r",
    fontSize: 12,
    color: color.dark,
  },
  id: {
    fontSize: 18,
    fontFamily: "n_b",
    marginBottom: 5,
  },
  textBold: {
    fontFamily: "n_sb",
    fontSize: 14,
  },
  imageContainer: {
    //backgroundColor: color.third + 'aa',
    width: 30,
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    // position: "absolute",
    // right: 20,
    // top: "50%",
    // transform: [{ translateY: -5 }],
    //elevation: 2,
  },
  image: {
    // tintColor: "#696969",
    tintColor: color.primary,
    width: 20,
    height: 20,
    resizeMode: "contain",
  },
  statusIcon: {
    width: 40,
    height: 40,
    resizeMode: "contain",
  },
});
