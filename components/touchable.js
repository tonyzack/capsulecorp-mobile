// Permet de créer un composant de clique en fonctiond de la plateforme

import { StyleSheet, Text, TouchableNativeFeedback, TouchableOpacity, View } from "react-native";
import React from "react";

import * as Constants from "expo-constants";

const Touchable = ({onPress, children}) => {
  if (Constants.default.platform.ios) {
    return <TouchableOpacity onPress={onPress}>
        {children}
    </TouchableOpacity>;
  }
  if (Constants.default.platform.android) {
    return <TouchableNativeFeedback onPress={onPress}>
        {children}
    </TouchableNativeFeedback>
  }
  return (
    <View>
      <Text>touchable</Text>
    </View>
  );
};

export default Touchable;

const styles = StyleSheet.create({});
