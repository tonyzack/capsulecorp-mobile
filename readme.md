## Lien APK
    https://nadhholy.me/capsule_corp.apk


# librairies et modules utilisés

## React Navigation
    [](https://reactnavigation.org)


## Redux 
    [](https://react-redux.js.org)


## Axios
    [](https://axios-http.com)


## Lottie
    [](https://github.com/lottie-react-native/lottie-react-native)

